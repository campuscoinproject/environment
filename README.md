# Environment
Everything in our environment will be code out (***Infrastructure as Code***) as much as possible that way the configuration the same for everyone.

What is Infrastructure as Code:
* https://en.wikipedia.org/wiki/Infrastructure_as_code
* https://docs.microsoft.com/en-us/azure/devops/learn/what-is-infrastructure-as-code

## Ubuntu 20.04.2.0 virtual machine environment setup

## Dependencies 

### Package Managers
* Windows: Chocolatey - https://chocolatey.org
* Mac: Homebrew - https://brew.sh

### On your Operating system (OS) must have the following installed

* VirtualBox 6.1 - https://www.virtualbox.org
* Hashicorp Packer 1.7.0 - https://www.packer.io
    * Packer installation instruction: https://learn.hashicorp.com/tutorials/packer/getting-started-install
        > Note: For Windows I would use the "Chocolatey Package Manager" and for Mac I would use "Homebrew Package Manager".
      


Within the root directory of environment run the following command.
```
packer build build.pkr.hcl
```
**_Currently not stable yet, build on your own risk!_**
