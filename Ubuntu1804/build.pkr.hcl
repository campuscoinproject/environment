# Ubuntu Instance Build for Virtual Box
variables {
  hostname = "ubuntu"
  domain = "local"
}

source "virtualbox-iso" "basic-example" {
  guest_os_type = "Ubuntu_64"
  iso_url = "https://releases.ubuntu.com/20.04.2.0/ubuntu-20.04.2.0-desktop-amd64.iso"
  iso_checksum = "93bdab204067321ff131f560879db46bee3b994bf24836bb78538640f689e58f"
  disk_size = "2000"
  memory = "1024"
  cpus = "1"
  guest_additions_mode = "upload"
  headless = "false"
  http_directory = "http"
  # iso_target_path = "iso/ubuntu-20.04.2.0-desktop-amd64.iso"
  output_directory = "vdi"
  vm_name = "CampusCoinDev"
  boot_command = [
    "<enter><enter><f6><esc><wait> ",
    "autoinstall ds=nocloud-net;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/",
    "<enter><wait>"
  ]
  shutdown_command = "shutdown -P now"
  ssh_username = "ubuntu"
  ssh_password = "ubuntu"
  ssh_pty = "true"
  ssh_timeout = "20m"
  ssh_handshake_attempts = "20"
}

build {
  sources = [
    "sources.virtualbox-iso.basic-example"]
  provisioner "shell-local" {
    inline = ["ls /"]
  }
}
