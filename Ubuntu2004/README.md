# CampusCoin Wallet Builder
 ## Reference Implementation
 
This repository contains the necessary files to build the CampusCoin wallets using an infrastructure-as-code approach with Packer, Vagrant, and VirtualBox.
## Prerequisites
Make sure you have the following software installed on your machine:
- [Packer](https://www.packer.io/downloads)
- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
## Getting Started
1. Clone this repository to your local machine.
2. Open a terminal or command prompt and navigate to the project directory.

### Building the Box
To build the CampusCoin virtual machine box, run the following command:
 
	packer build cc-reference.pkr.hcl

This will initiate the Packer build process, which will create a Vagrant-compatible box file.
### Importing the Box
Once the build process is complete, import the box to make it available for Vagrant:

	vagrant box add cc-reference output-myjammy/package.box

### Initializing and Running the Virtual Machine
From the same or another working directory, run the following command to initialize Vagrant:

	vagrant init cc-reference

This will create a Vagrantfile in the current directory.

To increase the memory and CPU allocation, open the generated Vagrantfile and insert the following configuration inside the `Vagrant.configure` block:

	config.vm.provider "virtualbox" do |vb|
	
	  vb.memory = 2048
	 
	  vb.cpus = 2
	
	  end
	
Save the file and exit the editor.

To start the virtual machine, run the following command:

	vagrant up --provider=virtualbox

### Accessing the Virtual Machine

After the virtual machine starts, perform a shutdown/restart from within VirtualBox to ensure stability.

To login to the reference implementation, use the following standard credentials for Vagrant boxes:

	Username: vagrant

	Password: vagrant

Navigate to the CampusCoin directory inside the virtual machine:

	cd /home/vagrant/campuscoin

### Building the CampusCoin Wallets
To build the CampusCoin wallets inside the virtual machine, run the following commands:

	sudo docker build . -f Dockerfile -t cc-test1

	sudo docker run -it cc-test1 bash

Inside the Docker container, you can find the **cc-cli** executable and check its version:

	find / -type f -name "cc-cli"

	cc-cli --version

### Cleanup
When you no longer need the virtual machine, perform the following cleanup steps:

* Shutdown the virtual machine by running **vagrant halt**.
* Get the vagrant id to remove by running **vagrant global-status** followed by running **vagrant destroy (7-digit alpha numeric ID)**.

* Remove the Vagrant box by running **vagrant box remove cc-reference**.

* Delete the generated output directory.

### Troubleshooting
If you encounter any issues during the setup process or while building the CampusCoin wallets, refer to the documentation of Packer, Vagrant, or VirtualBox for troubleshooting guidance.