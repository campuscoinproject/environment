#!/bin/bash

# Remove existing Docker installations
apt-get remove -y docker docker-engine docker.io containerd runc

# Install dependencies for Docker
apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Configure Docker repository
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update package lists and install Docker
apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
