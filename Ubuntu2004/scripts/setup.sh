#!/bin/bash -eux

# Create the CampusCoin directory within the VAGRANT user's home directory
mkdir ~/campuscoin && cd ~/campuscoin

# Download the CampusCoin Dockerfile
wget https://raw.githubusercontent.com/campuscoindev/CC/master/contrib/docker/Dockerfile
