# Define the "version" variable for the Packer build.
variable "version" {
  type    = string
  default = ""
}

# Define a local variable to generate a timestamp for use in the build process.
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

# Define the Vagrant source configuration for the "myjammy" box.
source "vagrant" "myjammy" {
  add_force    = true
  communicator = "ssh"
  provider     = "virtualbox"
  source_path  = "ubuntu/jammy64"
}

# Build configuration
build {
  sources = ["source.vagrant.myjammy"]

  # Provisioner for installing Docker
  provisioner "shell" {
    execute_command = "echo 'vagrant' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    script = "scripts/install_docker.sh"
  }

  # Provisioner for setting up CampusCoin
  provisioner "shell" {
    execute_command = "echo 'vagrant' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    script = "scripts/setup.sh"
  }
}
